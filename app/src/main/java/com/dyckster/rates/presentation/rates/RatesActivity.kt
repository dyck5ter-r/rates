package com.dyckster.rates.presentation.rates

import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.dyckster.rates.R
import com.dyckster.rates.presentation.base.BaseActivity
import com.dyckster.rates.presentation.model.RateViewModel
import com.dyckster.rates.presentation.rates.adapter.CurrencyListener
import com.dyckster.rates.presentation.rates.adapter.RatesAdapter
import com.dyckster.rates.util.hideKeyboard
import kotlinx.android.synthetic.main.activity_rate.*
import javax.inject.Inject
import javax.inject.Provider

class RatesActivity : BaseActivity(), RatesView, CurrencyListener {

    @Inject
    lateinit var provider: Provider<RatesPresenter>

    @InjectPresenter
    lateinit var presenter: RatesPresenter

    @ProvidePresenter
    fun providePresenter(): RatesPresenter = provider.get()

    private var adapter = RatesAdapter(this)
    private val scrollListener = ScrollListener()

    override fun layoutRes() = R.layout.activity_rate

    override fun viewCreated() {
        ratesRecycler.adapter = adapter
        ratesRecycler.itemAnimator?.changeDuration =
            RECYCLER_CHANGE_DURATION
        ratesRecycler.itemAnimator?.moveDuration =
            RECYCLER_MOVE_DURATION
        ratesRecycler.addOnScrollListener(scrollListener)
    }

    override fun displayRates(baseRate: RateViewModel, rates: List<RateViewModel>) {
        if (ratesRecycler.scrollState != RecyclerView.SCROLL_STATE_IDLE) {
            Log.d("Scroll", "Trying to update list but scroll state is not IDLE")
            return
        }
        adapter.updatesRates(baseRate, rates)
    }

    override fun onCurrencyChanged(value: Double) {
        presenter.onCurrencyChanged(value)
    }

    override fun onCurrencyClicked(currencyCode: String) {
        presenter.onCurrencyClicked(currencyCode)
    }

    override fun scrollToTop() {
        ratesRecycler.post {
            ratesRecycler.smoothScrollBy(0, Int.MIN_VALUE)
        }
    }

    override fun showRatesUpdateError() {
        Toast.makeText(this, R.string.error_rates_update, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        ratesRecycler.removeOnScrollListener(scrollListener)
    }

    inner class ScrollListener : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (dy > KEYBOARD_HIDE_THRESHOLD) {
                recyclerView.hideKeyboard()
            }
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) = Unit
    }

    companion object {

        private const val RECYCLER_CHANGE_DURATION = 0L
        private const val RECYCLER_MOVE_DURATION = 300L
        private const val KEYBOARD_HIDE_THRESHOLD = 10

    }

}
