package com.dyckster.rates.presentation.model

import java.math.BigDecimal

data class RateViewModel(
    val currencyCode: String,
    val value: BigDecimal,
    val baseRate: Double
)