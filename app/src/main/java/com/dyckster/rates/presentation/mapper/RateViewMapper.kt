package com.dyckster.rates.presentation.mapper

import com.dyckster.rates.domain.model.RateEntity
import com.dyckster.rates.presentation.model.RateViewModel
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

class RateViewMapper @Inject constructor() {

    fun transform(
        baseRateViewModel: RateViewModel,
        rateEntity: RateEntity
    ): RateViewModel {
        val rate = rateEntity.rate
        return RateViewModel(
            currencyCode = rateEntity.currencyCode,
            value = BigDecimal(baseRateViewModel.value.toDouble() * rate).setScale(2, RoundingMode.HALF_EVEN),
            baseRate = rate
        )
    }

    fun transformBase(baseEntity: RateEntity): RateViewModel {
        return RateViewModel(
            currencyCode = baseEntity.currencyCode,
            value = BigDecimal(BASE_VALUE).setScale(2, RoundingMode.HALF_EVEN),
            baseRate = baseEntity.rate
        )
    }

    fun updateWithNewValue(rateView: RateViewModel, newValue: Double): RateViewModel {
        return RateViewModel(
            currencyCode = rateView.currencyCode,
            value = BigDecimal(newValue).setScale(
                BASE_SCALE,
                RoundingMode.HALF_EVEN
            ),
            baseRate = rateView.baseRate
        )
    }

    fun updateWithNewBaseRate(rateView: RateViewModel, baseRate: Double, newValue: BigDecimal): RateViewModel {
        return RateViewModel(
            currencyCode = rateView.currencyCode,
            value = BigDecimal(baseRate).multiply(newValue).setScale(BASE_SCALE, RoundingMode.HALF_EVEN),
            baseRate = baseRate
        )
    }

    companion object {

        const val BASE_SCALE = 2
        const val BASE_VALUE = 100.00

    }
}