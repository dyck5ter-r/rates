package com.dyckster.rates.presentation.delegate

import java.math.BigDecimal

object MoneyBuilderDelegate {

    private const val EMPTY_VALUE = "0"
    private const val MAX_SCALE = 2

    /**
     * Takes value and converts it to BigDecimal value, scaled to 2 or 0. If passed value is whole number or end with both zeros,
     * then return value will be whole.
     * @param value  raw value to process in form of whole or floating point number. If passed value is not a number, then the
     * function would return 0.
     * @param lastValueEndedWithDot  flag that determines if value ending with dot need to be scaled to 0 or to 2
     * symbols after dot. For ex. if passed value is 100.0 and last value ended with dot, then function result would be 100.00;
     * in other case the returning value would be 100. Defaults to false
     */
    fun processMoneyInput(value: String, lastValueEndedWithDot: Boolean = false): BigDecimal {
        val correctedValue = if (value.isEmpty()) EMPTY_VALUE else value

        val bigDecimalValue =
            correctedValue.toBigDecimalOrNull()?.setScale(MAX_SCALE, BigDecimal.ROUND_HALF_EVEN) ?: BigDecimal.ZERO
        val isWhole = !correctedValue.contains(".")
        val hasEndingZeroes = (bigDecimalValue.toPlainString().endsWith("00") || correctedValue.endsWith("."))

        //If value has zeros at the end of string or number is whole - don't round it
        return if ((hasEndingZeroes && !lastValueEndedWithDot) || isWhole) {
            bigDecimalValue.setScale(0, BigDecimal.ROUND_HALF_EVEN)
        } else {
            bigDecimalValue
        }
    }

    fun findSelectorIndex(inputValue: String): Int {
        val indexOfDot = inputValue.indexOf(".")
        val symbolsAfterDot = inputValue.length - (indexOfDot + 1)
        val indexOfZero = inputValue.indexOfLast { it == '0' }
        return if (indexOfDot == -1 ||
            indexOfZero == -1 ||
            indexOfZero < indexOfDot ||
            (symbolsAfterDot == MAX_SCALE && !inputValue.endsWith("0"))
        ) inputValue.length
        else indexOfZero
    }

    fun getTextInputValue(moneyValue: BigDecimal, textEndsWithDot: Boolean): String {
        return "${moneyValue.toPlainString()}${if (textEndsWithDot) "." else ""}"
    }

}