package com.dyckster.rates.presentation.rates.adapter

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dyckster.rates.presentation.base.BaseViewHolder
import com.dyckster.rates.presentation.delegate.MoneyBuilderDelegate
import com.dyckster.rates.presentation.model.RateViewModel
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_rate.*
import java.util.*

class RatesViewHolder(
    private val listener: CurrencyListener,
    override val containerView: View
) : BaseViewHolder(containerView), LayoutContainer {

    private val moneyBuilder = MoneyBuilderDelegate
    private var rateModel: RateViewModel? = null
    private var previousValue = ""
    private var autoInput = false

    private var textWatcher = object : TextWatcher {
        override fun afterTextChanged(text: Editable?) {
            if (autoInput) {
                autoInput = false
                return
            }
            if (text == null) return
            val changedText = text.toString()

            val rateValue = rateModel?.value?.toPlainString()
            if (changedText == rateValue) return
            processMoneyInput(changedText)

            listener.onCurrencyChanged(changedText.toDoubleOrNull() ?: 0.0)
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
    }

    fun bind(rateModel: RateViewModel) {
        this.rateModel = rateModel
        val ctx = containerView.context
        val currencyCode = rateModel.currencyCode
        val imageResource = ctx.resources.getIdentifier(
            "$CURRENCY_DRAWABLE_PREFIX${currencyCode.toLowerCase()}", "drawable",
            ctx.packageName
        )
        Glide.with(containerView)
            .load(imageResource)
            .apply(RequestOptions.circleCropTransform())
            .into(currencyIcon)
        currencyNameShort.text = currencyCode
        currencyNameFull.text = Currency.getInstance(currencyCode).displayName
        processMoneyInput(rateModel.value.toPlainString(), false)

        currencySum.addTextChangedListener(textWatcher)
        currencySum.setOnFocusChangeListener { _, focused ->
            if (focused) listener.onCurrencyClicked(currencyCode)
        }
        containerView.setOnClickListener {
            currencySum.requestFocus()
            listener.onCurrencyClicked(currencyCode)
        }
    }

    private fun processMoneyInput(value: String, isAutoInput: Boolean = true) {
        if (isAutoInput) autoInput = true

        val moneyValue = moneyBuilder.processMoneyInput(value, previousValue.endsWith("."))
        val inputValue = moneyBuilder.getTextInputValue(moneyValue, value.endsWith("."))
        val inputSelection = moneyBuilder.findSelectorIndex(inputValue)

        previousValue = inputValue
        currencySum.setText(inputValue)
        currencySum.setSelection(inputSelection)
    }

    override fun unbind() {
        currencySum.removeTextChangedListener(textWatcher)
    }

    companion object {

        private const val CURRENCY_DRAWABLE_PREFIX = "curr_"

    }

}