package com.dyckster.rates.presentation.rates.adapter

interface CurrencyListener {

    fun onCurrencyChanged(value: Double)

    fun onCurrencyClicked(currencyCode: String)

}