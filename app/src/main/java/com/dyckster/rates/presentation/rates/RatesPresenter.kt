package com.dyckster.rates.presentation.rates

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.dyckster.rates.domain.interactor.RatesInteractor
import com.dyckster.rates.presentation.base.BasePresenter
import com.dyckster.rates.presentation.mapper.RateViewMapper
import com.dyckster.rates.presentation.model.RateViewModel
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@InjectViewState
class RatesPresenter @Inject constructor(
    private val ratesInteractor: RatesInteractor,
    private val rateMapper: RateViewMapper
) : BasePresenter<RatesView>() {

    private lateinit var baseRate: RateViewModel
    private var rates: List<RateViewModel> = ArrayList()
    private var filteredRates: List<RateViewModel> = ArrayList()

    private var updateSubject = PublishSubject.create<Boolean>()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        updateSubject.debounce(LIST_UPDATE_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .doOnNext {
                filteredRates = rates.filter { it.currencyCode != baseRate.currencyCode }
            }
            .observeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ updateScroll ->
                viewState.displayRates(baseRate, filteredRates)
                if (updateScroll) {
                    viewState.scrollToTop()
                }
            }, {
                Log.e(TAG, "Failed updating values in update subject", it)
                viewState.showRatesUpdateError()
            })
            .also { disposables.add(it) }

        Observable.interval(0L, REPEAT_TIME, TimeUnit.MILLISECONDS)
            .switchMapSingle {
                ratesInteractor.fetchRates(if (rates.isEmpty()) BASE_CURRENCY else baseRate.currencyCode)
            }
            .repeat()
            .map {
                if (rates.isEmpty()) {
                    val baseEntity = it[0]
                    baseRate = rateMapper.transformBase(baseEntity)
                }
                val ratesMap = it.associateBy { entry -> entry.currencyCode }

                val otherRates = if (rates.isEmpty()) it.map { entity ->
                    rateMapper.transform(baseRate, entity)
                } else {
                    rates.map { rate ->
                        rateMapper.updateWithNewBaseRate(
                            rate,
                            ratesMap[rate.currencyCode]?.rate ?: 1.0,
                            baseRate.value
                        )
                    }
                }
                rates = otherRates
                return@map otherRates
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                updateSubject.onNext(false)
            }, {
                Log.e(TAG, "Failed fetching rates", it)
                viewState.showRatesUpdateError()
            })
            .also { disposables.add(it) }
    }

    fun onCurrencyClicked(currencyCode: String) {
        rates.find { it.currencyCode == currencyCode }?.let {
            baseRate = it
        }
        updateSubject.onNext(true)
    }

    fun onCurrencyChanged(value: Double) {
        Single.fromCallable {
            baseRate = rateMapper.updateWithNewValue(baseRate, value)
            rates = rates.map { rateMapper.updateWithNewValue(it, value * it.baseRate) }
            filteredRates = rates.filter { it.currencyCode != baseRate.currencyCode }
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.displayRates(baseRate, filteredRates)
            }, {
                Log.e(TAG, "Failed changing rate values", it)
                viewState.showRatesUpdateError()
            })
            .also { disposables.add(it) }
    }

    companion object {

        private const val TAG = "RatesPresenter"

        private const val REPEAT_TIME = 1000L
        private const val LIST_UPDATE_DEBOUNCE_TIME = 100L

        private const val BASE_CURRENCY = "EUR"
    }
}