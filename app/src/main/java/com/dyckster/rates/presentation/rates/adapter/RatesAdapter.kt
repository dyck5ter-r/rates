package com.dyckster.rates.presentation.rates.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dyckster.rates.R
import com.dyckster.rates.presentation.model.RateViewModel

class RatesAdapter(private val listener: CurrencyListener) : RecyclerView.Adapter<RatesViewHolder>() {

    private var ratesList: List<RateViewModel> = ArrayList()

    fun updatesRates(baseRate: RateViewModel, rates: List<RateViewModel>) {
        Log.d("BaseRate", "Updating list with new base: ${baseRate.currencyCode}")
        val newList = listOf(baseRate, *rates.toTypedArray())
        val diffResult = DiffUtil.calculateDiff(RatesDiffUtil(newList, ratesList))
        this.ratesList = newList

        diffResult.dispatchUpdatesTo(this)
    }

    override fun onBindViewHolder(holder: RatesViewHolder, position: Int) {
        holder.bind(ratesList[position])
    }

    override fun onViewRecycled(holder: RatesViewHolder) {
        super.onViewRecycled(holder)
        holder.unbind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = RatesViewHolder(
        listener, LayoutInflater.from(parent.context).inflate(R.layout.item_rate, parent, false)
    )

    override fun getItemCount() = ratesList.size

    private inner class RatesDiffUtil(
        private val newList: List<RateViewModel>,
        private val oldList: List<RateViewModel>
    ) :
        DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val newItem = newList[newItemPosition]
            val oldItem = oldList[oldItemPosition]
            return oldItem.currencyCode == newItem.currencyCode
        }

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val newItem = newList[newItemPosition]
            val oldItem = oldList[oldItemPosition]

            if (oldItemPosition == 0 && newItemPosition == 0) {
                return true
            } else {
                return oldItem.value == newItem.value
            }

        }
    }
}