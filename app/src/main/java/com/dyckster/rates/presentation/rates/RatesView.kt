package com.dyckster.rates.presentation.rates

import com.arellomobile.mvp.MvpView
import com.dyckster.rates.presentation.model.RateViewModel

interface RatesView : MvpView {

    fun displayRates(baseRate: RateViewModel, rates: List<RateViewModel>)

    fun scrollToTop()

    fun showRatesUpdateError()

}