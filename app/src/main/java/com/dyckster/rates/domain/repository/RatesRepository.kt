package com.dyckster.rates.domain.repository

import com.dyckster.rates.data.model.RateModel
import io.reactivex.Single

interface RatesRepository {

    fun getRates(baseCurrency: String): Single<RateModel>

}