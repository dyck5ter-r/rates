package com.dyckster.rates.domain.model

data class RateEntity(
    val currencyCode: String,
    val rate: Double
)