package com.dyckster.rates.domain.mapper

import com.dyckster.rates.data.model.RateModel
import com.dyckster.rates.domain.model.RateEntity
import javax.inject.Inject

class RateEntityMapper @Inject constructor() {

    fun transform(rateModel: RateModel): List<RateEntity> {
        val ratesList: MutableList<RateEntity> = ArrayList()
        ratesList.add(RateEntity(rateModel.baseCurrency, 1.0))
        rateModel.rates.onEach { e ->
            ratesList.add(RateEntity(e.key, e.value))
        }
        return ratesList
    }

}