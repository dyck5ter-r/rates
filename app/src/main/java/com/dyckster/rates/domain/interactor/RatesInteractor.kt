package com.dyckster.rates.domain.interactor

import com.dyckster.rates.domain.mapper.RateEntityMapper
import com.dyckster.rates.domain.model.RateEntity
import com.dyckster.rates.domain.repository.RatesRepository
import io.reactivex.Single
import javax.inject.Inject

class RatesInteractor @Inject constructor(
    private val ratesRepository: RatesRepository,
    private val rateEntityMapper: RateEntityMapper
) {

    fun fetchRates(currency: String): Single<List<RateEntity>> {
        return ratesRepository.getRates(currency)
            .map { rateEntityMapper.transform(it) }
    }

}