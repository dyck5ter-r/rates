package com.dyckster.rates.data.repository

import com.dyckster.rates.data.model.RateModel
import com.dyckster.rates.data.network.RatesApi
import com.dyckster.rates.domain.repository.RatesRepository
import io.reactivex.Single

class AppRatesRepository(private val ratesApi: RatesApi) : RatesRepository {

    override fun getRates(baseCurrency: String): Single<RateModel> {
        return ratesApi.requestRates(baseCurrency)
    }
}