package com.dyckster.rates.data.model

import com.google.gson.annotations.SerializedName

data class RateModel(
    @SerializedName("base")
    val baseCurrency: String,
    @SerializedName("date")
    val updateDate: String,
    @SerializedName("rates")
    val rates: Map<String, Double>
)