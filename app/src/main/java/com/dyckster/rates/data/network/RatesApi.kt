package com.dyckster.rates.data.network

import com.dyckster.rates.data.model.RateModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {

    @GET("latest")
    fun requestRates(@Query("base") baseRate: String): Single<RateModel>

}