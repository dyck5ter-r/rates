package com.dyckster.rates.di

import javax.inject.Scope

@Scope
annotation class PerActivity

@Scope
annotation class PerApplication
