package com.dyckster.rates.di.modules

import android.content.Context
import com.dyckster.rates.App
import com.dyckster.rates.di.PerApplication
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

    @PerApplication
    @Provides
    fun provideContext(application: App): Context = application

}