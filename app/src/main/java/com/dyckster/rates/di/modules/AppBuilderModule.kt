package com.dyckster.rates.di.modules

import com.dyckster.rates.di.PerActivity
import com.dyckster.rates.presentation.rates.RatesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface AppBuilderModule {

    @PerActivity
    @ContributesAndroidInjector
    fun provideRatesActivityFactory(): RatesActivity

}