package com.dyckster.rates.di.modules

import com.dyckster.rates.data.network.RatesApi
import com.dyckster.rates.data.repository.AppRatesRepository
import com.dyckster.rates.domain.repository.RatesRepository
import dagger.Module
import dagger.Provides

@Module(includes = [NetworkModule::class])
class RepositoryModule {

    @Provides
    fun provideRatesRepository(ratesApi: RatesApi): RatesRepository = AppRatesRepository(ratesApi)

}