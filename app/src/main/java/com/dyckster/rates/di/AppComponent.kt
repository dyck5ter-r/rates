package com.dyckster.rates.di

import com.dyckster.rates.App
import com.dyckster.rates.di.modules.AppBuilderModule
import com.dyckster.rates.di.modules.ApplicationModule
import com.dyckster.rates.di.modules.NetworkModule
import com.dyckster.rates.di.modules.RepositoryModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector

@PerApplication
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppBuilderModule::class,
        ApplicationModule::class,
        RepositoryModule::class,
        NetworkModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()

}