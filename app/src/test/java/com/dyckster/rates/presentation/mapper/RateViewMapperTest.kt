package com.dyckster.rates.presentation.mapper

import com.dyckster.rates.domain.model.RateEntity
import com.dyckster.rates.presentation.model.RateViewModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode

class RateViewMapperTest {

    private lateinit var rateViewMapper: RateViewMapper

    @Before
    fun setUp() {
        rateViewMapper = RateViewMapper()
    }


    @Test
    fun transform() {
        val baseRate = RateViewModel("RUB", BigDecimal(100.00), 1.0)
        val rateEntity = RateEntity("USD", 0.014)

        val transformedRate = rateViewMapper.transform(baseRate, rateEntity)

        assertEquals("USD", transformedRate.currencyCode)
        assertEquals(BigDecimal(1.4).setScale(RateViewMapper.BASE_SCALE, RoundingMode.HALF_EVEN), transformedRate.value)
    }

    @Test
    fun transformBase() {
        val rateEntity = RateEntity("EUR", 0.75)
        val transformed = rateViewMapper.transformBase(rateEntity)

        assertEquals("EUR", transformed.currencyCode)
        assertEquals(
            BigDecimal(RateViewMapper.BASE_VALUE).setScale(RateViewMapper.BASE_SCALE, RoundingMode.HALF_EVEN),
            transformed.value
        )
        assertEquals(0.75, transformed.baseRate, 0.0)
    }

    @Test
    fun updateWithNewValue() {
        val rate = RateViewModel("RUB", BigDecimal(100.00), 0.15)
        val newValue = 10.0

        val updatedValue = rateViewMapper.updateWithNewValue(rate, newValue)

        assertEquals(BigDecimal(10.0).setScale(2, RoundingMode.HALF_EVEN), updatedValue.value)
        assertEquals(0.15, updatedValue.baseRate, 0.0)
    }

    @Test
    fun updateWithNewBaseRate() {
        val rate = RateViewModel("RUB", BigDecimal(100.00), 1.0)
        val newValue = BigDecimal(10.0)

        val updatedValue = rateViewMapper.updateWithNewBaseRate(rate, 0.15, newValue)

        assertEquals(BigDecimal(1.5).setScale(2, RoundingMode.HALF_EVEN), updatedValue.value)
        assertEquals(0.15, updatedValue.baseRate, 0.0)
    }

}