package com.dyckster.rates.presentation.delegate

import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

class MoneyBuilderDelegateTest {

    @Test
    fun `process money inputs without knowing previous value`() {
        val testedValues = listOf(
            "100",
            "100.",
            "100.0",
            "100.00",
            "100.10",
            "100.01",
            "100.11",
            "100.111"
        )
        val expectedValues = arrayOf(
            BigDecimal(100.00).setScale(0, RoundingMode.HALF_EVEN),
            BigDecimal(100.00).setScale(0, RoundingMode.HALF_EVEN),
            BigDecimal(100.00).setScale(0, RoundingMode.HALF_EVEN),
            BigDecimal(100.00).setScale(0, RoundingMode.HALF_EVEN),
            BigDecimal(100.10).setScale(2, RoundingMode.HALF_EVEN),
            BigDecimal(100.01).setScale(2, RoundingMode.HALF_EVEN),
            BigDecimal(100.11).setScale(2, RoundingMode.HALF_EVEN),
            BigDecimal(100.11).setScale(2, RoundingMode.HALF_EVEN)
        )
        val actualValue = testedValues.map { MoneyBuilderDelegate.processMoneyInput(it) }.toTypedArray()

        println(Arrays.toString(actualValue))
        Assert.assertArrayEquals(expectedValues, actualValue)
    }

    @Test
    fun `process money inputs knowing previous value`() {
        val testedValues = listOf(
            "100",
            "100.",
            "100.0",
            "100.00",
            "100.10",
            "100.01",
            "100.11",
            "100.111"
        )
        val expectedValues = arrayOf(
            BigDecimal(100.00).setScale(0, RoundingMode.HALF_EVEN),
            BigDecimal(100.00).setScale(2, RoundingMode.HALF_EVEN),
            BigDecimal(100.00).setScale(2, RoundingMode.HALF_EVEN),
            BigDecimal(100.00).setScale(2, RoundingMode.HALF_EVEN),
            BigDecimal(100.10).setScale(2, RoundingMode.HALF_EVEN),
            BigDecimal(100.01).setScale(2, RoundingMode.HALF_EVEN),
            BigDecimal(100.11).setScale(2, RoundingMode.HALF_EVEN),
            BigDecimal(100.11).setScale(2, RoundingMode.HALF_EVEN)
        )
        val actualValue = testedValues.map { MoneyBuilderDelegate.processMoneyInput(it, true) }.toTypedArray()

        println(Arrays.toString(actualValue))
        Assert.assertArrayEquals(expectedValues, actualValue)
    }

    @Test
    fun `money builder error handling test`() {
        val testedValues = listOf("hello", "100HHH~")

        val valuesAfterTest = testedValues.map { MoneyBuilderDelegate.processMoneyInput(it) }.toTypedArray()

        val expectedResults = arrayOf(BigDecimal.ZERO, BigDecimal.ZERO)
        println(valuesAfterTest)

        Assert.assertArrayEquals(valuesAfterTest, expectedResults)
    }

    @Test
    fun `input value test`() {
        val textWithDot = MoneyBuilderDelegate.getTextInputValue(
            BigDecimal(111).setScale(0, RoundingMode.HALF_EVEN),
            true
        )
        val textWithoutDot =
            MoneyBuilderDelegate.getTextInputValue(
                BigDecimal(111.11).setScale(2, RoundingMode.HALF_EVEN),
                false
            )

        Assert.assertEquals(textWithDot, "111.")
        Assert.assertEquals(textWithoutDot, "111.11")
    }

    @Test
    fun `selector index test`() {
        val testedValues = arrayOf("100", "100.", "111.01", "1111.15", "133.70")
        val expectedIndexes = arrayOf(3, 4, 6, 7, 5)
        val actualIndexes = testedValues.map { MoneyBuilderDelegate.findSelectorIndex(it) }.toTypedArray()

        println(Arrays.toString(actualIndexes))
        Assert.assertArrayEquals(expectedIndexes, actualIndexes)

    }
}