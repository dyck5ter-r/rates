var fs = require('fs');
var path = require('path');
var process = require("process");

fs.readdir(__dirname, function (err, files) {
  if (err) {
    console.error("Could not list the directory.", err);
    process.exit(1);
  }

  files.forEach(function (file, index) {
    // Make one pass and make the file complete
    let ext = file.split('.').pop();
    if (ext === "webp"){
      fs.rename(file,"curr_"+file)
    }
  });
});
